﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GPSManager : MonoBehaviour
{
    [SerializeField]
    private Text log;

    public static GPSManager Instance { set; get; }

    public GPSLocation position;
    public Vector3 positionAccuracy;

    public float heading;
    public float headingAccuracy;

    public bool UseFakeLocation;

    public static bool initializedGPS = false;
    public Transform arCamera;

    public static Action<GPSLocation> OnGetGPSPostion = delegate { };

    [HideInInspector]
    public bool isRunning = true;

    [HideInInspector]
    public LocationServiceStatus ServiceStatus = LocationServiceStatus.Stopped;

    public float latitude;
    public float longitude;
    public float altitude;
    public bool isInstance = false;
    public float dCamera = 0;

    private void Awake()
    {
        Instance = this;
        DontDestroyOnLoad(gameObject);
        StartCoroutine(StartLocationService());
    }

    private void Update()
    {
        latitude = Input.location.lastData.latitude;
        longitude = Input.location.lastData.longitude;
        altitude = Input.location.lastData.altitude;
        position = new GPSLocation(latitude, altitude, longitude);
        dCamera = Vector3.Distance(arCamera.position, Vector3.zero);
        heading = Input.compass.trueHeading;
        log.text = string.Format("Init GPS : {0} \nCurrent GPS: {1}\nDistance move: {2} \nInit heading: {3} \nCurrent heading: {4}\nIsIntance: {5} ",
            GPSLocation.initCurrentLocation, position, dCamera, GPSLocation.heading, heading, isInstance);
    }

    private IEnumerator StartLocationService()
    {
        ServiceStatus = LocationServiceStatus.Initializing;
        
        if (UseFakeLocation)
        {
            //log.text = string.Format("Using fake GPS location lat:{0} lon:{1} height:{2}", position.x, position.y, position.y);
            Debug.Log(string.Format("Using fake GPS location lat:{0} lon:{1} height:{2}", position.latitude, position.longitude, position.altitude));
            ServiceStatus = LocationServiceStatus.Running;
            yield break;
        }

        Input.compass.enabled = true;

        yield return new WaitForSeconds(5);

        if (!Input.location.isEnabledByUser)
        {
            //log.text = "User has not enabled gps";
            Debug.Log("User has not enabled gps");
            yield break;
        }

        Input.location.Start(1f,0.1f);

        yield return new WaitForSeconds(3);

        int maxWait = 20;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        if (maxWait <= 0)
        {
            log.text = "Timed out";
            Debug.Log("Timed Out");
            yield break;
        }

        ServiceStatus = Input.location.status;
        
        if (Input.location.status == LocationServiceStatus.Failed)
        {
            //log.text = "Unable to dtermine device location";
            Debug.Log("Unable to dtermine device location");
            yield break;
        }

        latitude = Input.location.lastData.latitude;
        longitude = Input.location.lastData.longitude;
        altitude = Input.location.lastData.altitude;
        position = new GPSLocation(latitude, altitude, longitude);

        var hAcc = Input.location.lastData.horizontalAccuracy;
        var vAcc = Input.location.lastData.verticalAccuracy;
        positionAccuracy = new Vector3(hAcc, vAcc, hAcc);


        while (isRunning)
        {
            UpdateGPS();
            
            yield return new WaitForSeconds(0.05f);
        }
    }

    private void UpdateGPS()
    {
        if (Input.location.status == LocationServiceStatus.Running)
        {
            latitude = Input.location.lastData.latitude;
            longitude = Input.location.lastData.longitude;
            altitude = Input.location.lastData.altitude;
            position = new GPSLocation(latitude, altitude, longitude);

            ServiceStatus = Input.location.status;
            //log.text = string.Format("Lat: {0} Long: {1} Alt: {2}\nDir: {3}", position.x, position.z, position.y, heading);
            Debug.Log(string.Format("Lat: {0} Long: {1} Alt: {2}\nDir: {3}", position.latitude, position.longitude, position.altitude, heading));

            if (log)
            {
                //log.text = string.Format("Lat: {0} Long: {1} Alt: {2}\nDir: {3}", position.latitude, position.longitude, position.altitude, heading);
            }

            if (!initializedGPS)
            {
                while (Input.compass.headingAccuracy < 0)
                {
                    if (Input.compass.headingAccuracy >= 0)
                        break;
                }
                initializedGPS = true;
               
                GPSLocation.heading = Input.compass.trueHeading;
                GPSLocation.initCurrentLocation = position;
                OnGetGPSPostion(GPSLocation.initCurrentLocation);
                isInstance = true;
                //log.text = "GPS: " + GPSLocation.initCurrentLocation;
                // OnGetGPSPostion(GPSLocation.initCurrentLocation);
            }
            else
            {
                //dCamera = Vector3.Distance(arCamera.position, Vector3.zero);

                //if (!isInstance &&  dCamera > 5)
                //{

                //    var gpsDelta = position - GPSLocation.initCurrentLocation;
                //    var sinHeading = (gpsDelta.latitude / arCamera.position.x - gpsDelta.longitude / arCamera.position.z) / (arCamera.position.z / arCamera.position.z + arCamera.position.x / arCamera.position.z);
                //    var cosHeading = (gpsDelta.latitude / arCamera.position.z + gpsDelta.longitude / arCamera.position.x) / (arCamera.position.z / arCamera.position.z + arCamera.position.x / arCamera.position.z);

                //    if (cosHeading == Mathf.Cos(Mathf.Asin(sinHeading)))
                //        GPSLocation.heading = Mathf.Asin(sinHeading);
                //    else
                //        GPSLocation.heading = Mathf.PI - Mathf.Asin(sinHeading);

                //    //log.text = string.Format("sin: {0}, cos: {1}", sinHeading, cosHeading);
                //    isInstance = true;
                //    OnGetGPSPostion(GPSLocation.initCurrentLocation);
                //}
                //else
                //{
                //    //log.text = string.Format("{0}, {1} distance {2}", position.latitude, position.longitude, GPSLocation.Distance(GPSLocation.initCurrentLocation, position));
                //}
            }
        }
    }

    //public void ToggleGPSPause(Button button)
    //{
    //    _isGPSPaused = !_isGPSPaused;
    //    button.GetComponentInChildren<Text>().text = _isGPSPaused ? "Unpause GPS" : "Pause GPS";
    //}

    //public void ToggleCompassPause(Button button)
    //{
    //    _isCompassPaused = !_isCompassPaused;            
    //    button.GetComponentInChildren<Text>().text = _isCompassPaused ? "Unpause Compass" : "Pause Compass";
    //}
}
