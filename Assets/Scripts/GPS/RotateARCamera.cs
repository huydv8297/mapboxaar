﻿using Mapbox.Unity.Location;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RotateARCamera : MonoBehaviour {

    public Transform arRoot;
    public float heading;
    public Text log;
    public ILocationProvider locationProvider;
    public bool isRotate = false;
    public string init;

    void Start () {
        locationProvider = LocationProviderFactory.Instance.DefaultLocationProvider;
        StartCoroutine(UpdateRotate());
    }
	
	// Update is called once per frame
	IEnumerator UpdateRotate () {
        while (true)
        {
            yield return new WaitForSeconds(0.1f);
            log.text = init + "/n /n" + locationProvider.CurrentLocation.Accuracy + " , " + locationProvider.CurrentLocation.DeviceOrientation + ", " + locationProvider.CurrentLocation.UserHeading;
            if (!isRotate && locationProvider.CurrentLocation.Accuracy > 0 && locationProvider.CurrentLocation.DeviceOrientation != 0)
            {
                yield return new WaitForSeconds(3f);
                isRotate = true;
                heading = locationProvider.CurrentLocation.DeviceOrientation;
                arRoot.transform.rotation = Quaternion.Euler(0, heading, 0);
                init = locationProvider.CurrentLocation.Accuracy + " , " + locationProvider.CurrentLocation.DeviceOrientation + ", " + locationProvider.CurrentLocation.UserHeading;
            }
        }
    }
        
}
