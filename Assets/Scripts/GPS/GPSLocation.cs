﻿using UnityEngine;

public class GPSLocation
{
    public float latitude { get; set; }
    public float longitude { get; set; }
    public float altitude { get; set; }
    public static float heading { get; set; }
    public static GPSLocation initCurrentLocation;

    public GPSLocation(float latitude, float altitude, float longitude)
    {
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
    }

    public GPSLocation(Vector3 position)
    {
        this.latitude = position.x;
        this.longitude = position.z;
        this.altitude = position.y;
    }

    public static GPSLocation operator- (GPSLocation a, GPSLocation b)
    {
        return new GPSLocation(a.latitude - b.latitude, a.altitude - b.altitude, a.longitude - b.longitude);
    }

    public static float Distance(GPSLocation a, GPSLocation b)
    { 
        return Mathf.Sqrt(Mathf.Pow((a.latitude - b.latitude), 2) + Mathf.Pow((a.altitude - b.altitude), 2) + Mathf.Pow((a.longitude - b.longitude), 2));
    }

    public override string ToString()
    {
        return string.Format("({0}, {1}, {2})", latitude, altitude, longitude);
    }
}

