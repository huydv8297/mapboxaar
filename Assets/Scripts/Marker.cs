﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Marker : MonoBehaviour {

    public RectTransform bgImage;
    public BoxCollider colider;
	// Use this for initialization
	void Start () {
        NavigationManager.Instance.listMarker.Add(transform);
        Vector3 bgSize = colider.size;
        bgSize.x = bgImage.rect.width * 0.08f;

        colider.size = bgSize;

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnMouseDown()
    {
        NavigationManager.Instance.SetDestination(transform.position);
    }
}
