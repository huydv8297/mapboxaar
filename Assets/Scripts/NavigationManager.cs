using Mapbox.Directions;
using Mapbox.Unity;
using Mapbox.Unity.Map;
using Mapbox.Unity.Utilities;
using Mapbox.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavigationManager : MonoBehaviour {

    private static NavigationManager instance;

    /// <summary>
    /// The singleton instance.
    /// </summary>
    public static NavigationManager Instance
    {
        get
        {
            return instance;
        }
    }

    #region Variables

    [SerializeField]
    AbstractMap map;

    [SerializeField]
    Transform[] waypoints;
    private List<Vector3> cachedWaypoints;

    [SerializeField]
    [Range(1, 10)]
    private float UpdateFrequency = 2;

    private Directions directions;
    private int counter;

    [SerializeField]
    private bool recalculateNext;

    [SerializeField]
    LineRenderer lineRenderer;

    [SerializeField]
    LineRenderer mapLineRenderer;

    [SerializeField]
    Camera arCamera;

    [SerializeField]
    Camera mapCamera;

    [SerializeField]
    GameObject navigationIcon;

    [SerializeField]
    GameObject currentPositionIcon;

    [SerializeField]
    GameObject cursor;

    [SerializeField]
    GameObject destination;

    [SerializeField]
    GameObject player;

    [SerializeField]
    GameObject searchPanel;

    [SerializeField]
    GameObject buttonOn;

    [SerializeField]
    GameObject buttonOff;

    [SerializeField]
    float smooth = 0.2f;

    public List<Transform> listMarker = new List<Transform>();

    Vector3 currentPosition;
    public bool isNavigationMode = false;
    public float heading;


    List<Vector3> listArrow = new List<Vector3>();

    #endregion

    protected virtual void Awake()
    {

        if (instance == null)
        {
            instance = this;
        }

        if (map == null)
        {
            map = FindObjectOfType<AbstractMap>();
        }
        directions = MapboxAccess.Instance.Directions;
        map.OnUpdated += Query;
    }

    public void Start()
    {
        
        cachedWaypoints = new List<Vector3>(waypoints.Length);
        foreach (var item in waypoints)
        {
            cachedWaypoints.Add(item.position);
        }
        recalculateNext = false;
        
        StartCoroutine(QueryTimer());

        //SetDestination(new Vector3(10,1,100));
    }

    protected virtual void OnDestroy()
    {
        map.OnUpdated -= Query;
    }

    void Query()
    {
        var count = waypoints.Length;
        var wp = new Vector2d[count];



        for (int i = 0; i < count; i++)
        {
            //heading = map.transform.rotation.eulerAngles.y;
            //waypoints[i].RotateAround(new Vector3(0, 0, 0), Vector3.up, -heading);
            
            wp[i] = waypoints[i].GetGeoPosition(map.CenterMercator, map.WorldRelativeScale);
            //waypoints[i].position = cachedWaypoints[i];

        }
        print(waypoints[1].transform.position + ", " + wp[1]);
        //wp[1] = new Vector2d(21.018182, 105.773166);
        var directionResource = new DirectionResource(wp, RoutingProfile.Driving);
        directionResource.Steps = true;
        directions.Query(directionResource, HandleDirectionsResponse);
    }

    void UpdateScale(float room)
    {
        player.transform.localScale = new Vector3(room, 1, room);
        destination.transform.localScale = new Vector3(room, 1, room);
    }

    public void TurnOnOffSearchPanel()
    {
        searchPanel.SetActive(!searchPanel.activeSelf);
    }

    public IEnumerator QueryTimer()
    {
        while (true)
        {
            yield return new WaitForSeconds(UpdateFrequency);
            for (int i = 0; i < waypoints.Length; i++)
            {
                if (waypoints[i].position != cachedWaypoints[i])
                {
                    recalculateNext = true;
                    cachedWaypoints[i] = waypoints[i].position;
                }
            }
            VectorLayerProperties vectorLayerProperties = ((VectorLayer)map.VectorData).LayerProperty;
            if (recalculateNext)
            {
                Query();
                recalculateNext = false;
            }
        }
    }

    void HandleDirectionsResponse(DirectionsResponse response)
    {
        if (response == null || null == response.Routes || response.Routes.Count < 1)
        {
            return;
        }

        listArrow.Clear();
        foreach (var point in response.Routes[0].Geometry)
        {
            listArrow.Add(Conversions.GeoToWorldPosition(point.x, point.y, map.CenterMercator, map.WorldRelativeScale).ToVector3xz());
        }
        mapLineRenderer.positionCount = listArrow.Count;
        mapLineRenderer.SetPositions(listArrow.ToArray());

        lineRenderer.positionCount = listArrow.Count;
        lineRenderer.SetPositions(listArrow.ToArray());
    }

    public void SetDestination(Vector2d destination)
    {

        waypoints[0].position = arCamera.transform.position;
        waypoints[1].position = Conversions.GeoToWorldPosition(destination.x, destination.y, map.CenterMercator, map.WorldRelativeScale).ToVector3xz();
        Debug.Log("set destination: " + waypoints[1].position);
        Debug.Log("set destination: " + destination);
        searchPanel.SetActive(false);
        Query();
    }

    public void SetDestination(Vector3 destination)
    {
        if (!isNavigationMode)
            return;

        waypoints[0].position = arCamera.transform.position;
        waypoints[1].position = destination;
        Query();
    }

    public void ReturnCurrentPosition()
    {
        currentPosition = arCamera.transform.position;
        currentPosition.y = mapCamera.transform.localPosition.y;
        isReturnCurrentPosition = true;
        //mapCamera.transform.position = currentPosition;
    }

    bool isReturnCurrentPosition = false;
    private void Update()
    {
        if (isReturnCurrentPosition )
        {
            var tempMapCamera = mapCamera.transform.position;
            tempMapCamera.y = arCamera.transform.position.y;
            if (Vector3.Distance(tempMapCamera, arCamera.transform.position) < 0.2f)
            {
                mapCamera.transform.position = currentPosition;
                isReturnCurrentPosition = false;
            }
            mapCamera.transform.position = Vector3.Lerp(mapCamera.transform.position, currentPosition, smooth);
        }
    }

    public void ChangeNavigationMode(bool status)
    {
        isNavigationMode = status;
        cursor.SetActive(!cursor.activeSelf);
        destination.SetActive(!destination.activeSelf);

        if(isNavigationMode)
        {

            buttonOn.SetActive(true);
            buttonOff.SetActive(false);

            lineRenderer.enabled = true;
            mapLineRenderer.enabled = true;
            var temp = mapCamera.transform.position;
            temp.y = 1;

            destination.transform.position = temp;
            SetDestination(destination.transform.position);
        }
        else
        {

            buttonOn.SetActive(false);
            buttonOff.SetActive(true);

            lineRenderer.enabled = false;
            mapLineRenderer.enabled = false;
        }
    }



    public void EnableMap()
    {
        if(isNavigationMode)
        {
            cursor.SetActive(false);
            destination.SetActive(true);
        }
        else
        {
            cursor.SetActive(true);
            destination.SetActive(false);
        }
    }

}
