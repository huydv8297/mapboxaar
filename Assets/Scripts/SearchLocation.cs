﻿using Mapbox.Geocoding;
using Mapbox.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SearchLocation : MonoBehaviour
{

    public InputField input;
    public bool isSearching;
    public Transform content;
    public GameObject resultPrefab;

    ForwardGeocodeResource resource;

    List<Feature> features;

    void Start()
    {
        resource = new ForwardGeocodeResource("");
    }

    public void OnTextChange(string msg)
    {
        print(input.text);
        if (input.text.Length != 0)
        {
            Search(input.text);
        }
        else
        {
            ResetResult();
        }
    }

    public void Search(string key)
    {
        isSearching = true;
        if (!string.IsNullOrEmpty(key))
        {
            resource.Query = key;
            MapboxAccess.Instance.Geocoder.Geocode(resource, HandleGeocoderResponse);
        }
    }

    public void HandleGeocoderResponse(ForwardGeocodeResponse res)
    {
        //null if no internet connection
        if (res != null)
        {
            //null if invalid token
            if (res.Features != null)
            {
                features = res.Features;
                DisplayResult(features);
            }
        }
        isSearching = false;
    }

    public void DisplayResult(List<Feature> features)
    {
        ResetResult();
        foreach (Feature feature in features)
        {
            GameObject result = Instantiate(resultPrefab, content);
            LocationResult lable = result.GetComponent<LocationResult>();
            lable.SetValue(feature.PlaceName, feature.Geometry.Coordinates);

        }
    }

    public void ResetResult()
    {
        foreach(Transform child in content)
        {
            Destroy(child.gameObject);
        }
        content.DetachChildren();
    }
}
