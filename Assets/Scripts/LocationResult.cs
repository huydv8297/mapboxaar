﻿using Mapbox.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class LocationResult : MonoBehaviour {

    public Text labelText;
    
    public Vector2d latlong;
	
    public void SetValue(string name, Vector2d latlong)
    {
        labelText.text = name;
        this.latlong = latlong;
}

	public void Onclick()
    {
        NavigationManager.Instance.SetDestination(latlong);
    }
}
