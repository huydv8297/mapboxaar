﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zoom : MonoBehaviour {

    [SerializeField]
    Transform mapCamera;

    [SerializeField]
    float size;

    public void ZoomIn()
    {
        mapCamera.position -= new Vector3(0, size, 0);
    }

    public void ZoomOut()
    {
        mapCamera.position += new Vector3(0, size, 0);
    }
}
