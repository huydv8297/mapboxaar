﻿using Mapbox.Unity.Location;
using Mapbox.Unity.Utilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMapbox : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        ILocationProvider locationProvider = LocationProviderFactory.Instance.DeviceLocationProvider;
        Location location = locationProvider.CurrentLocation;
        string msg = string.Format("LatLong: {0}\nUserHeading: {1}\nDeviceOrientation: {2}\nAccuracy: {3}\nTimestamp: {4}",
            location.LatitudeLongitude, location.UserHeading, location.DeviceOrientation, location.Accuracy, location.Timestamp);
        Console.Instance.Log(msg, "blue");
        locationProvider.OnLocationUpdated += OnLocationUpdated;

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnLocationUpdated(Location location)
    {
        //Location currentLocation = locationProvider.CurrentLocation;
        string msg = string.Format("LatLong: {0}\nUserHeading: {1}\nDeviceOrientation: {2}\nAccuracy: {3}\nTimestamp: {4}",
            location.LatitudeLongitude, location.UserHeading, location.DeviceOrientation, location.Accuracy, location.Timestamp);
        Console.Instance.Log(msg, "blue");
    }
}
