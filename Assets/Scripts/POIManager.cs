﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class POIManager : MonoBehaviour
{

    public List<Vector3> listPOI = new List<Vector3>();
    public List<GameObject> listPOIObject = new List<GameObject>();
    public GameObject prefab;
    public Transform origin;
    public Transform cameraTransform;
    public float scale = 1f;
    public Text log;
    public GetNearbyLocation getNearbyLocation;
    public float head = 0;


    public bool isInit = false;

    private void Start()
    {
        GPSManager.OnGetGPSPostion += Init;
        //Init(new GPSLocation(21.01732f, 105.7809f, 0, head));
    }

    public void Init(GPSLocation gpsLocation)
    {
        
        log.text += "Init";
        if (!isInit && Input.location.status == LocationServiceStatus.Running)
        {
            isInit = true;
            getNearbyLocation.requestData(gpsLocation.latitude, gpsLocation.longitude, 100);

            GetNearbyLocation.OnGetDataSuccess += list =>
            {
                foreach (var poi in list)
                {
                    GameObject obj = Instantiate(prefab);
                    obj.transform.parent = origin;
                    obj.name = poi.ToString();
                    obj.GetComponent<Label>().SetTitle(poi.name);

                    UnityGPSCoordinateSystemSynchrony(obj.transform, new GPSLocation(poi.lat, 0, poi.lng), gpsLocation);
                    
                }
                log.text = "Tìm thấy  " + list.Count.ToString() + " địa điểm ở xung quanh";
            };

            //GameObject obj = Instantiate(prefab, Vector3.zero, Quaternion.identity, origin);
            //listPOIObject.Add(obj);
            //UnityGPSCoordinateSystemSynchrony(obj.transform, gpsLocation);
            //log.text += "Instantiate " + obj.transform.position + ", ";
        }
    }

    public Vector3 ConvertGPSToUnity(GPSLocation obj, GPSLocation gpsLocation)
    {
        float latitude = obj.latitude;
        float longitude = obj.longitude;

        Debug.Log("GPS" + obj);
        // Conversion factors
        float degreesLatitudeInMeters = 111132;
        float degreesLongitudeInMetersAtEquator = 111319.9f;

        // Real GPS Position - This will be the world origin.
        var gpsLat = gpsLocation.latitude;
        var gpsLon = gpsLocation.longitude;

        // GPS position converted into unity coordinates
        var latOffset = (latitude - gpsLat) * degreesLatitudeInMeters;
        var lonOffset = (longitude - gpsLon) * degreesLongitudeInMetersAtEquator * Mathf.Cos(latitude * (Mathf.PI / 180));

        return new Vector3(latOffset, gpsLocation.altitude, lonOffset);
    }

    public void UnityGPSCoordinateSystemSynchrony(Transform transform, GPSLocation obj, GPSLocation gpsLocation)
    {
        float constDegreeToRadian = Mathf.PI / 180;
        Vector3 rotatedEulerAngle = new Vector3(0, - GPSLocation.heading, 0);
        Debug.Log("rotatedEulerAngle" + rotatedEulerAngle);
        
        Debug.Log("Position ConvertGPSToUnity: " + ConvertGPSToUnity(obj, gpsLocation));
        transform.position = ConvertGPSToUnity(obj, gpsLocation);
        
        Vector3 position = transform.position;
        //log.text = position.ToString() + " heading = " + GPSLocation.initCurrentLocation.heading;
        //Rotate X-axis
        position.y = position.y * Mathf.Cos(constDegreeToRadian * rotatedEulerAngle.x) - position.z * Mathf.Sin(constDegreeToRadian * rotatedEulerAngle.x);
        position.z = position.y * Mathf.Sin(constDegreeToRadian * rotatedEulerAngle.x) + position.z * Mathf.Cos(constDegreeToRadian * rotatedEulerAngle.x);

        //Rotate Y-axis
        position.x = position.z * Mathf.Sin(constDegreeToRadian * rotatedEulerAngle.y) + position.x * Mathf.Cos(constDegreeToRadian * rotatedEulerAngle.y);
        position.z = position.z * Mathf.Cos(constDegreeToRadian * rotatedEulerAngle.y) - position.x * Mathf.Sin(constDegreeToRadian * rotatedEulerAngle.y);

        //Rotate Z-axis
        position.x = position.x * Mathf.Cos(constDegreeToRadian * rotatedEulerAngle.z) - position.y * Mathf.Sin(constDegreeToRadian * rotatedEulerAngle.z);
        position.y = position.x * Mathf.Sin(constDegreeToRadian * rotatedEulerAngle.z) + position.y * Mathf.Cos(constDegreeToRadian * rotatedEulerAngle.z);

        transform.localScale = scale * Vector3.Distance(position, cameraTransform.position) * transform.localScale;
        log.text = "GPS: " + gpsLocation + "\n RawPosition: " + transform.position + "\n Position: " + position + "\n Scale: " + transform.localScale;

        position.y = cameraTransform.position.y;

        transform.position = position;
        Debug.Log("Position UnityGPSCoordinateSystemSynchrony: " + transform.position);
        //var refPoint = aRReferencePoint.AddReferencePoint(new Pose(position, transform.rotation));
        //transform.parent = refPoint.transform;
    }
}
