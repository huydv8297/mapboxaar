﻿using System;

namespace GoogleAPI
{
    [Serializable]
    public class Location : Object
    {

        public float lat;
        public float lng;
        public string name;

        public Location(float lt, float lg, string nm)
        {
            lat = lt;
            lng = lg;
            name = nm;
        }

        public override String ToString()
        {
            return name + " (" + lat + ",  " + lng + ")";
        }
    }
}
