﻿namespace DecodeGooglePolyline
{
    public class CoordinateEntity
    {
        public double Latitude { get; internal set; }
        public double Longitude { get; internal set; }
    }
}