﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestNav : MonoBehaviour
{
    public GameObject prefab;
    public float space = 0.3f;
    public float prefabWidth;
    public Transform start;
    public Transform end;

    void Start()
    {
        prefabWidth = prefab.GetComponent<Renderer>().bounds.size.z;
        CreateNavigation(start.position, end.position);
    }


    public void CreateNavigation(Vector3 startPosition, Vector3 endPosition)
    {
        var distance = Vector3.Distance(startPosition, endPosition);
        int count = (int)(distance / (prefabWidth + space));

        count = count > 50 ? 50 : count;
        for (int j = 0; j < count; ++j)
        {
            GameObject obj = Instantiate(prefab);
            obj.transform.parent = transform;
            obj.transform.LookAt(end);
            obj.transform.Rotate(Vector3.left, 90f);
            float percent = (float)j / count;
            var temp = Vector3.Lerp(startPosition, endPosition, percent);
            temp.y = 1;
            obj.transform.localPosition = temp;
        }
    }
}
