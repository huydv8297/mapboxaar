﻿using System;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using UnityEngine;
using UnityEngine.Networking;
using GoogleAPI;

public class GetNearbyLocation : MonoBehaviour
{

    private string _dataString;

    private string _nextPageToken; //get more data
    private List<Location> listLocation;
    public static Action<List<Location>> OnGetDataSuccess;

    // Start is called before the first frame update
    void Awake()
    {
        listLocation = new List<Location>();
    }

    public void requestData(float lat, float lng, float radius)
    {
        listLocation.Clear();
        StartCoroutine(GetData(lat, lng, radius));
    }

    private IEnumerator GetData(float lat, float lng, float radius)
    {
        UnityWebRequest www = UnityWebRequest.Get(UrlApiConfig.searchNearyBy(lat, lng, radius));
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            print(www.error);
        }
        else
        {
            //print(www.downloadHandler.text);
            _dataString = www.downloadHandler.text;
        }

        if (www.isDone)
        {
            HandleData(_dataString);
        }
        
    }

    private void HandleData(string dataString)
    {
        var data = JSON.Parse(dataString);

        var res = data["results"];
       
        for (var i = 0; i<res.Count; i++ )
        {
            var lat = res[i]["geometry"]["location"]["lat"].AsFloat;
            var lng = res[i]["geometry"]["location"]["lng"].AsFloat;
            var nm = res[i]["name"];
            
            var loc = new Location(lat, lng, nm);
           
            listLocation.Add(loc);
        }

        OnGetDataSuccess(listLocation);
    }
}
