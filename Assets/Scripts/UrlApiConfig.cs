﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UrlApiConfig
{
    public const string ApiKey = "AIzaSyCwdruVeP3qNBI2cKcNPzEJ6YnfVBRjlQc";
    public static string NearbySearch = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?";

    public static string searchNearyBy(float lat, float lng, float radius)
    {
        var url = NearbySearch + "location=" + lat + "," + lng + "&radius=" + radius+"&key=" + ApiKey;
        return url;
    }
    
}
