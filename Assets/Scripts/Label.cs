﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Label : MonoBehaviour {

    public Transform camera;
    public TextMesh title;
    public Transform prefab;
	void Start () {
        camera = Camera.main.transform;
	}

	
	// Update is called once per frame
	void Update () {
        transform.LookAt(camera);
        transform.Rotate(new Vector3(0, 0, -30));
        //transform.localScale = Vector3.Distance(transform.position, camera.position) * prefab.localScale;
    }

    public void SetTitle(string name)
    {
        title.text = name;
    }
}
