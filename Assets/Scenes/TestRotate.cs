﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestRotate : MonoBehaviour {

    public float heading;
	void Start () {
        transform.RotateAround(new Vector3(0, 0, 0), Vector3.up, heading);
    }
	
	// Update is called once per frame
	void Update () {
        
	}
}
