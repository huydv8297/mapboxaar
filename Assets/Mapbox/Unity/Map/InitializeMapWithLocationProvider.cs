namespace Mapbox.Unity.Map
{
	using System.Collections;
	using Mapbox.Unity.Location;
	using UnityEngine;

	public class InitializeMapWithLocationProvider : MonoBehaviour
	{
		[SerializeField]
		AbstractMap _map;

		ILocationProvider _locationProvider;
    
		private void Awake()
		{
			// Prevent double initialization of the map. 
			_map.InitializeOnStart = false;
            //StartCoroutine(ReturnLog(1));
		}

        IEnumerator ReturnLog(float time)
        {
            yield return new WaitForSeconds(time)
;
            Input.location.Start();
            // Start the compass.
            Input.compass.enabled = true;
            //_map.transform.rotation = Quaternion.Euler(0, -Input.compass.trueHeading, 0);
            //float angle = Quaternion.Angle(_map.transform.rotation, Quaternion.Euler(0, -Input.compass.trueHeading, 0));

            /*var xrot = Mathf.Atan2(Input.acceleration.z, Input.acceleration.y);
            var yzmag = Mathf.Sqrt(Mathf.Pow(Input.acceleration.y, 2) + Mathf.Pow(Input.acceleration.z, 2));
            var zrot = Mathf.Atan2(Input.acceleration.x, yzmag);

            var xangle = xrot * (180 / Mathf.PI) + 90;
            var zangle = -zrot * (180 / Mathf.PI);
            _map.transform.eulerAngles = new Vector3(xangle, 0, zangle - Input.compass.trueHeading);*/

            Vector3  vector = new Vector3(transform.localEulerAngles.x, -Input.compass.trueHeading, transform.localEulerAngles.z);
            _map.transform.localEulerAngles = vector;

           // Utilities.Console.Instance.Log("trueHeading : " + Input.compass.trueHeading + " device: " + location.DeviceOrientation, "white");
        }
            
        protected virtual IEnumerator Start()
		{
			yield return null;
			_locationProvider = LocationProviderFactory.Instance.DefaultLocationProvider;
			_locationProvider.OnLocationUpdated += LocationProvider_OnLocationUpdated; ;
		}

		void LocationProvider_OnLocationUpdated(Unity.Location.Location location)
		{
			_locationProvider.OnLocationUpdated -= LocationProvider_OnLocationUpdated;
			_map.Initialize(location.LatitudeLongitude, _map.AbsoluteZoom);
		}
	}
}
